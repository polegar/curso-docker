FROM python:3

COPY requirements.txt /app/

RUN pip install -r /app/requirements.txt

COPY app.py /app/

EXPOSE 5000
EXPOSE 8080

CMD ["python", "/app/app.py"]
